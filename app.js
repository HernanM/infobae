let noticiaAPublicar = null

function publicar2() {

    crearNoticias()

    if (noticiaAPublicar !== null) {
        switch (noticiaAPublicar.categoria) {
            case "0": // principal
                {
                    let nodeSection = document.getElementById("principal")
                    let nodehijo = nodeSection.children[0]
                    nodehijo.remove()

                    let nodeArticle = document.createElement("article")
                    nodeArticle.className = "p-1"

                    let nodeA = document.createElement("a")
                    nodeA.href = "#"
                    let nodeP = document.createElement("p")
                    let nodeTitulo = document.createElement("h1")
                    let nodeImg = document.createElement("img")
                    let nodeBr = document.createElement("hr")

                    let contenidoTitulo = document.createTextNode(noticiaAPublicar.titulo)
                    nodeTitulo.appendChild(contenidoTitulo)

                    let contenidoParrafo = document.createTextNode(noticiaAPublicar.subtitulo)
                    nodeP.appendChild(contenidoParrafo)

                    nodeImg.src = noticiaAPublicar.imagen
                    nodeImg.style = "width: 100%;"

                    nodeA.appendChild(nodeTitulo)
                    nodeA.appendChild(nodeP)
                    nodeA.appendChild(nodeImg)

                    nodeArticle.appendChild(nodeA)
                    nodeArticle.appendChild(nodeBr)

                    nodeSection.appendChild(nodeArticle)

                }
                break;

            case "1": //aside principal
                {
                    crear(noticiaAPublicar,"aside-principal","p-1")
                }
                break;
            case "2": //Secundarias
                {
                    crear(noticiaAPublicar,"secundarias","d-flex flex-column flex-grow-1 m-2")
                }
                break;
            case "3": //accesoria
                {
                    crear(noticiaAPublicar,"accesorias","d-flex flex-column flex-grow-1 m-2")
                }
                break;




            default:
                break;
        }

        noticiaApublicar = null
    }

}

function crearNoticias() {

    let titulo = document.getElementById("titulo").value
    let subtitulo = document.getElementById("subtitulo").value
    let imagen = document.getElementById("imagen").value
    let categoria = document.getElementById("categoria").value


    if (titulo !== "" && subtitulo !== "" && imagen !== "") {

        let noticia = {
            titulo: titulo,
            subtitulo: subtitulo,
            imagen: imagen,
            categoria: categoria,
        }

        noticiaAPublicar = noticia

        document.getElementById("titulo").value = null
        document.getElementById("subtitulo").value = null
        document.getElementById("imagen").value = null
        document.getElementById("categoria").value = null
    }
    else {
        alert("Faltan rellenar campos")
    }

}






// function publicar() {

//     //for (let i = 0; i < 3; i++) {
//     if (noticias.length !== 0) {

//         let nodeSection = document.createElement("section")
//         nodeSection.className = "d-flex flex-column flex-lg-row"

//         noticias.forEach(noticia => {

//             let nodeArticle = document.createElement("article")
//             nodeArticle.className = "d-flex flex-column flex-grow-1 m-2"

//             let nodeTitulo = document.createElement("h2")
//             let nodeImg = document.createElement("img")
//             let nodeBr = document.createElement("hr")

//             let contenidoTitulo = document.createTextNode(noticia.titulo)
//             nodeTitulo.appendChild(contenidoTitulo)


//             nodeImg.src = noticia.imagen
//             nodeImg.style = "width: 100%;"

//             nodeArticle.appendChild(nodeTitulo)
//             nodeArticle.appendChild(nodeImg)
//             nodeArticle.appendChild(nodeBr)

//             nodeSection.appendChild(nodeArticle)


//         });

//         document.getElementById("seccionNoticias").appendChild(nodeSection);
//         //}
//         noticias = []
//     }
//     else {
//         alert("no hay noticias ")
//     }
// }

function crear(noticia, seccion, clase) {
    
    let nodeSection = document.getElementById(seccion)
    let articulos = Array.from(document.getElementById(seccion).children)

    nodeSection.innerHTML = ""

    articulos.pop()

    let nodeArticle = document.createElement("article")
    nodeArticle.className = clase

    let nodeTitulo = document.createElement("h2")
    let nodeImg = document.createElement("img")
    let nodeHr = document.createElement("hr")

    let contenidoTitulo = document.createTextNode(noticia.titulo)
    nodeTitulo.appendChild(contenidoTitulo)

    nodeImg.src = noticia.imagen
    nodeImg.style = "width: 100%;"

    nodeArticle.appendChild(nodeTitulo)
    nodeArticle.appendChild(nodeImg)
    nodeArticle.appendChild(nodeHr)

    articulos.unshift(nodeArticle)

    articulos.forEach(articulo => {
        nodeSection.appendChild(articulo)
    })
}


